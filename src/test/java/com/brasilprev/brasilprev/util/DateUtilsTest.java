package com.brasilprev.brasilprev.util;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class DateUtilsTest {

    @Test
    public void calcularDiferencaEntreDatas() {
        assertThat(DateUtils.dateDiffDay(LocalDate.of(2022, 05, 01), LocalDate.of(2021, 05, 01))).isEqualTo(365);
    }
}
