package com.brasilprev.brasilprev.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResgateForm {

    @NotNull
    @JsonProperty("idPlano")
    private Long idPlano;

    @NotNull
    @JsonProperty("valorResgate")
    private BigDecimal valorResgate;
}
