package com.brasilprev.brasilprev.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlanoForm {

    @NotNull
    @JsonProperty("idCliente")
    private Long idCliente;

    @NotNull
    @JsonProperty("idProduto")
    private Long idProduto;

    @NotNull
    @JsonProperty("aporte")
    private BigDecimal aporte;

    @NotNull
    @JsonProperty("dataDaContratacao")
    private LocalDate dataDaContratacao;
}
