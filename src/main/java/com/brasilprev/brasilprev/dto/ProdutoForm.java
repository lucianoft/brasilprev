package com.brasilprev.brasilprev.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProdutoForm {

    @NotNull
    @JsonProperty("nome")
    private String nome;

    @NotNull
    @JsonProperty("susep")
    private String susep;

    @NotNull
    @JsonProperty("expiracaoDeVenda")
    private LocalDate expiracaoDeVenda;

    @NotNull
    @JsonProperty("valorMinimoAporteInicial")
    private BigDecimal valorMinimoAporteInicial;

    @NotNull
    @JsonProperty("valorMinimoAporteExtra")
    private BigDecimal valorMinimoAporteExtra;

    @NotNull
    @JsonProperty("idadeDeEntrada")
    private Integer idadeDeEntrada;

    @NotNull
    @JsonProperty("idadeDeSaida")
    private Integer idadeDeSaida;

    @JsonProperty("carenciaInicialDeResgate")
    private Integer carenciaInicialDeResgate;

    @JsonProperty("carenciaEntreResgates")
    private Integer carenciaEntreResgates;
}
