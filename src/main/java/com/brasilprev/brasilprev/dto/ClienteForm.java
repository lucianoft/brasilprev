package com.brasilprev.brasilprev.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClienteForm {

    @NotNull
    @JsonProperty("cpf")
    private Long cpf;

    @NotNull
    @JsonProperty("nome")
    private String nome;

    @Email
    @JsonProperty("email")
    private String email;

    @NotNull
    @JsonProperty("dataDeNascimento")
    private LocalDate dataDeNascimento;

    @NotNull
    @JsonProperty("sexo")
    private String sexo;

    @NotNull
    @JsonProperty("rendaMensal")
    private BigDecimal rendaMensal;
}
