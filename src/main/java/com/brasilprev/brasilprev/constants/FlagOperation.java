package com.brasilprev.brasilprev.constants;

public enum FlagOperation {
    INSERT,
    UPDATE
}
