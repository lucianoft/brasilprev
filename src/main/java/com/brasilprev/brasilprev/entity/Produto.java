package com.brasilprev.brasilprev.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name="TB_PRODUTO")
@Data
@Builder()
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"}, callSuper = false)
@EntityListeners(AuditingEntityListener.class)
public class Produto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_produto", nullable = false)
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "susep")
    private String susep;

    @Column(name = "expiracao_venda")
    private LocalDate expiracaoDeVenda;

    @Column(name = "valor_minimo_aporte_inicial")
    private BigDecimal valorMinimoAporteInicial;

    @Column(name = "valor_minimo_aporte_extra")
    private BigDecimal valorMinimoAporteExtra;

    @Column(name = "idade_entrada")
    private Integer idadeDeEntrada;

    @Column(name = "idade_saida")
    private Integer idadeDeSaida;

    @Column(name = "carencia_inicial_resgate")
    private Integer carenciaInicialDeResgate;

    @Column(name = "carencia_entre_resgates")
    private Integer carenciaEntreResgates;

    @Column(name = "dt_inclusao", nullable = false)
    @CreatedDate
    private LocalDateTime dataInclusao;

    @Column(name = "dt_alteracao")
    @LastModifiedDate
    private LocalDateTime dataAlteracao;

}
