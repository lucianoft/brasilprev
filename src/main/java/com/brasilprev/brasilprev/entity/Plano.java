package com.brasilprev.brasilprev.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name="TB_PLANO")
@Data
@Builder()
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"}, callSuper = false)
@EntityListeners(AuditingEntityListener.class)
public class Plano {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_plano", nullable = false)
    private Long id;

    @ManyToOne(targetEntity=Cliente.class)
    @JoinColumn(name="id_cliente", referencedColumnName="id_cliente", nullable=false)
    private Cliente cliente;

    @ManyToOne(targetEntity=Produto.class)
    @JoinColumn(name="id_produto", referencedColumnName="id_produto", nullable=false)
    private Produto produto;

    @Column(name = "aporte")
    private BigDecimal aporte;

    @Column(name = "data_contratacao")
    private LocalDate dataDaContratacao;

    @Column(name = "dt_inclusao", nullable = false)
    @CreatedDate
    private LocalDateTime dataInclusao;

    @Column(name = "dt_alteracao")
    @LastModifiedDate
    private LocalDateTime dataAlteracao;

}
