package com.brasilprev.brasilprev.entity;

import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name="TB_RESGATE")
@Data
@Builder()
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"}, callSuper = false)
@EntityListeners(AuditingEntityListener.class)
public class Resgate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_resgate", nullable = false)
    private Long id;

    @ManyToOne(targetEntity=Plano.class)
    @JoinColumn(name="id_plano", referencedColumnName="id_plano", nullable=false)
    private Plano plano;

    @Column(name = "valor_resgate")
    private BigDecimal valorResgate;

    @Column(name = "dt_inclusao", nullable = false)
    @CreatedDate
    private LocalDateTime dataInclusao;

    @Column(name = "dt_alteracao")
    @LastModifiedDate
    private LocalDateTime dataAlteracao;
}
