package com.brasilprev.brasilprev.entity;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name="TB_CLIENTE")
@Data
@Builder()
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"}, callSuper = false)
@EntityListeners(AuditingEntityListener.class)
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_cliente", nullable = false)
    private Long id;

    @Column(name = "cpf")
    private Long cpf;

    @Column(name = "nome")
    private String nome;

    @Column(name = "email")
    private String email;

    @Column(name = "data_nascimento")
    private LocalDate dataDeNascimento;

    @Column(name = "sexo")
    private String sexo;

    @Column(name = "renda_mensal")
    private BigDecimal rendaMensal;

    @Column(name = "dt_inclusao", nullable = false)
    @CreatedDate
    private LocalDateTime dataInclusao;

    @Column(name = "dt_alteracao")
    @LastModifiedDate
    private LocalDateTime dataAlteracao;

}
