package com.brasilprev.brasilprev.service;

import com.brasilprev.brasilprev.constants.FlagOperation;
import com.brasilprev.brasilprev.dto.ProdutoForm;
import com.brasilprev.brasilprev.dto.ProdutoResponse;
import com.brasilprev.brasilprev.entity.Produto;
import com.brasilprev.brasilprev.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class ProdutoService extends CrudService<Produto, Long> {

    @Autowired
    private ProdutoRepository clientRepository;

    @Override
    CrudRepository<Produto, Long> getRepository() {
        return clientRepository;
    }

    @Override
    protected void validar(Produto entity, FlagOperation flagOperation) {
    }

    public ProdutoResponse insert(ProdutoForm form) {
        Produto Produto = com.brasilprev.brasilprev.entity.Produto.builder()
        .nome(form.getNome())
        .susep(form.getSusep())
        .expiracaoDeVenda(form.getExpiracaoDeVenda())
        .valorMinimoAporteInicial(form.getValorMinimoAporteInicial())
        .valorMinimoAporteExtra(form.getValorMinimoAporteExtra())
        .idadeDeEntrada(form.getIdadeDeEntrada())
        .idadeDeSaida(form.getIdadeDeSaida())
        .carenciaInicialDeResgate(form.getCarenciaInicialDeResgate())
        .carenciaEntreResgates(form.getCarenciaEntreResgates())
                .build();

        Produto = insert(Produto);

        return ProdutoResponse.builder().id(Produto.getId()).build();
    }
}
