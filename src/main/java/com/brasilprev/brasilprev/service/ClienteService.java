package com.brasilprev.brasilprev.service;

import com.brasilprev.brasilprev.constants.FlagOperation;
import com.brasilprev.brasilprev.dto.ClienteResponse;
import com.brasilprev.brasilprev.dto.ClienteForm;
import com.brasilprev.brasilprev.entity.Cliente;
import com.brasilprev.brasilprev.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class ClienteService extends CrudService<Cliente, Long> {

    @Autowired
    private ClienteRepository clientRepository;

    @Override
    CrudRepository<Cliente, Long> getRepository() {
        return clientRepository;
    }

    @Override
    protected void validar(Cliente entity, FlagOperation flagOperation) {
    }

    public ClienteResponse insert(ClienteForm form) {
        Cliente cliente = Cliente.builder().cpf(form.getCpf())
                .dataDeNascimento(form.getDataDeNascimento())
                .email(form.getEmail())
                .nome(form.getNome())
                .sexo(form.getSexo())
                .rendaMensal(form.getRendaMensal())
                .build();

        cliente = insert(cliente);

        return ClienteResponse.builder().id(cliente.getId()).build();
    }
}
