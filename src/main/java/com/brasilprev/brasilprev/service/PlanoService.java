package com.brasilprev.brasilprev.service;

import com.brasilprev.brasilprev.constants.FlagOperation;
import com.brasilprev.brasilprev.dto.PlanoForm;
import com.brasilprev.brasilprev.dto.PlanoResponse;
import com.brasilprev.brasilprev.entity.Cliente;
import com.brasilprev.brasilprev.entity.Plano;
import com.brasilprev.brasilprev.entity.Produto;
import com.brasilprev.brasilprev.repository.ClienteRepository;
import com.brasilprev.brasilprev.repository.PlanoRepository;
import com.brasilprev.brasilprev.repository.ProdutoRepository;
import com.brasilprev.brasilprev.util.DateUtils;
import com.brasilprev.brasilprev.util.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class PlanoService extends CrudService<Plano, Long> {

    @Autowired
    private PlanoRepository planoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private ProdutoRepository produtoRepository;

    @Override
    CrudRepository<Plano, Long> getRepository() {
        return planoRepository;
    }

    @Override
    protected void validar(Plano entity, FlagOperation flagOperation) {
        if (FlagOperation.INSERT.equals(flagOperation)){
            Produto produto = entity.getProduto();
            Cliente cliente = entity.getCliente();
            if (DateUtils.after(entity.getDataDaContratacao(), produto.getExpiracaoDeVenda())){
                throw new RuntimeException("Produto está espirado, não é possivel realizar a contrataçao");
            }

            if (!NumberUtils.isGreaterThan(entity.getAporte(), produto.getValorMinimoAporteInicial())){
                throw new RuntimeException("Valor Minito de Aporte não foi atingido, não é possivel realizar a contrataçao");
            }
            int idade = DateUtils.idade(cliente.getDataDeNascimento());
            if (idade < produto.getIdadeDeEntrada()){
                throw new RuntimeException("Cliente não possui idade Minima Permidade, não é possivel realizar a contrataçao");
            }
            if (idade > produto.getIdadeDeSaida()){
                throw new RuntimeException("Cliente possui idade acima da Permidade, não é possivel realizar a contrataçao");
            }
        }
    }

    public PlanoResponse insert(PlanoForm form) {
        Cliente cliente = clienteRepository.findById(form.getIdCliente()).orElseThrow( () -> new RuntimeException("Não foi encontrado Cliente para Id " + form.getIdCliente()) );
        Produto produto = produtoRepository.findById(form.getIdProduto()).orElseThrow( () -> new RuntimeException("Não foi encontrado Produto para Id " + form.getIdProduto()) );

        Plano plano = Plano.builder()
                .cliente(cliente)
                .produto(produto)
                .aporte(form.getAporte())
                .dataDaContratacao(form.getDataDaContratacao())
                .build();

        plano = insert(plano);

        return PlanoResponse.builder().id(plano.getId()).build();
    }

    public Plano resgate(Plano plano, BigDecimal vrResgate){
        plano.setAporte(plano.getAporte().subtract(vrResgate));
        return update(plano);
    }

    public Plano aporteExtra(Plano plano, BigDecimal vrAporte){
        plano.setAporte(plano.getAporte().add(vrAporte));
        return update(plano);
    }
}
