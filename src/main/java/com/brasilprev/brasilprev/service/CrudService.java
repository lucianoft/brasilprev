package com.brasilprev.brasilprev.service;

import com.brasilprev.brasilprev.constants.FlagOperation;
import org.springframework.data.repository.CrudRepository;

import java.io.Serializable;

public abstract class CrudService<T, ID extends Serializable> {

    abstract CrudRepository<T, ID> getRepository();

    protected abstract void validar(T entity, FlagOperation flagOperation);

    public T insert(T entity) {
        validar(entity, FlagOperation.INSERT);
        return getRepository().save(entity);
    }

    public T update(T entity) {
        validar(entity, FlagOperation.UPDATE);
        return getRepository().save(entity);
    }

    public T findById(ID id) {
        if (id != null) {
            return getRepository().findById(id).orElse(null);
        }
        return null;
    }
}
