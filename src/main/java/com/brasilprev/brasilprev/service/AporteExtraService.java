package com.brasilprev.brasilprev.service;

import com.brasilprev.brasilprev.constants.FlagOperation;
import com.brasilprev.brasilprev.dto.AporteExtraForm;
import com.brasilprev.brasilprev.dto.AporteExtraResponse;
import com.brasilprev.brasilprev.entity.Cliente;
import com.brasilprev.brasilprev.entity.AporteExtra;
import com.brasilprev.brasilprev.entity.Plano;
import com.brasilprev.brasilprev.entity.Produto;
import com.brasilprev.brasilprev.repository.ClienteRepository;
import com.brasilprev.brasilprev.repository.AporteExtraRepository;
import com.brasilprev.brasilprev.repository.PlanoRepository;
import com.brasilprev.brasilprev.repository.ProdutoRepository;
import com.brasilprev.brasilprev.util.DateUtils;
import com.brasilprev.brasilprev.util.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class AporteExtraService extends CrudService<AporteExtra, Long> {

    @Autowired
    private AporteExtraRepository aporteExtraRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private PlanoRepository planoRepository;

    @Autowired
    private ProdutoRepository produtoRepository;

    @Autowired
    private PlanoService planoService;

    @Override
    CrudRepository<AporteExtra, Long> getRepository() {
        return aporteExtraRepository;
    }

    @Override
    protected void validar(AporteExtra entity, FlagOperation flagOperation) {
        if (FlagOperation.INSERT.equals(flagOperation)){
            Plano plano = entity.getPlano();
            Produto produto = plano.getProduto();

            if (NumberUtils.isLessThan(entity.getValorAporte(), produto.getValorMinimoAporteExtra())){
                throw new RuntimeException("Valor Minimo de Aporte Inicial não foi atingido, não é possivel realizar a operaçao");
            }
            planoService.aporteExtra(plano, entity.getValorAporte());
        }
    }

    public AporteExtraResponse insert(AporteExtraForm form) {
        Cliente cliente = clienteRepository.findById(form.getIdCliente()).orElseThrow( () -> new RuntimeException("Não foi encontrado Cliente para Id " + form.getIdCliente()) );
        Plano plano = planoRepository.findById(form.getIdPlano()).orElseThrow( () -> new RuntimeException("Não foi encontrado Plano para Id " + form.getIdPlano()) );

        AporteExtra aporteExtra = AporteExtra.builder()
                .cliente(cliente)
                .plano(plano)
                .valorAporte(form.getValorAporte())
                .build();

        aporteExtra = insert(aporteExtra);

        return AporteExtraResponse.builder().id(aporteExtra.getId()).build();
    }
}
