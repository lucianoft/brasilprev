package com.brasilprev.brasilprev.service;

import com.brasilprev.brasilprev.constants.FlagOperation;
import com.brasilprev.brasilprev.dto.ResgateForm;
import com.brasilprev.brasilprev.dto.ResgateResponse;
import com.brasilprev.brasilprev.entity.Resgate;
import com.brasilprev.brasilprev.entity.Cliente;
import com.brasilprev.brasilprev.entity.Plano;
import com.brasilprev.brasilprev.entity.Produto;
import com.brasilprev.brasilprev.repository.ResgateRepository;
import com.brasilprev.brasilprev.repository.ClienteRepository;
import com.brasilprev.brasilprev.repository.PlanoRepository;
import com.brasilprev.brasilprev.repository.ProdutoRepository;
import com.brasilprev.brasilprev.util.DateUtils;
import com.brasilprev.brasilprev.util.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class ResgateService extends CrudService<Resgate, Long> {

    @Autowired
    private ResgateRepository resgateRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private PlanoRepository planoRepository;

    @Autowired
    private ProdutoRepository produtoRepository;

    @Autowired
    private PlanoService planoService;

    @Override
    CrudRepository<Resgate, Long> getRepository() {
        return resgateRepository;
    }

    @Override
    protected void validar(Resgate resgate, FlagOperation flagOperation) {
        if (FlagOperation.INSERT.equals(flagOperation)){
            Plano plano = resgate.getPlano();
            Produto produto = plano.getProduto();

            if (NumberUtils.isGreaterThan(resgate.getValorResgate(), plano.getAporte())){
                throw new RuntimeException("Valor Resgate ultrapassa o valor em Aporte, não é possivel realizar a operaçao");
            }
            validarPeriodoResgate(resgate);
            planoService.resgate(plano, resgate.getValorResgate());
        }
    }

    private void validarPeriodoResgate(Resgate resgate) {
        Plano plano = resgate.getPlano();
        Produto produto = plano.getProduto();
        Resgate ultimoResgate = resgateRepository.findTop1ByPlanoId(plano.getId());
        if (ultimoResgate == null){
            long qtdeDias = DateUtils.dateDiffDay(LocalDate.now(), plano.getDataDaContratacao());
            if (qtdeDias < produto.getCarenciaInicialDeResgate()){
                throw new RuntimeException("Não foi atingida a qtde de dias mininas para primeiro resgate, não é possivel realizar a operaçao");
            }
        }else{
            long qtdeDias = DateUtils.dateDiffDay(LocalDate.now(), ultimoResgate.getDataInclusao().toLocalDate());
            if (qtdeDias < produto.getCarenciaInicialDeResgate()){
                throw new RuntimeException("Não foi atingida a qtde de dias mininas entre resgates, não é possivel realizar a operaçao");
            }
        }
    }

    public ResgateResponse insert(ResgateForm form) {
        Plano plano = planoRepository.findById(form.getIdPlano()).orElseThrow( () -> new RuntimeException("Não foi encontrado Plano para Id " + form.getIdPlano()) );

        Resgate resgate = Resgate.builder()
                .plano(plano)
                .valorResgate(form.getValorResgate())
                .build();

        resgate = insert(resgate);

        return ResgateResponse.builder().id(resgate.getId()).build();
    }
}
