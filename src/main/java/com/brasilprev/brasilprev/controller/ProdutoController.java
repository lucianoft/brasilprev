package com.brasilprev.brasilprev.controller;

import com.brasilprev.brasilprev.dto.ProdutoForm;
import com.brasilprev.brasilprev.dto.ProdutoResponse;
import com.brasilprev.brasilprev.service.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    public ResponseEntity<ProdutoResponse> insert(
            @RequestBody @Valid ProdutoForm model) {
        return ResponseEntity.status(HttpStatus.CREATED).body(produtoService.insert(model));

    }
}
