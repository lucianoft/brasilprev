package com.brasilprev.brasilprev.controller;

import com.brasilprev.brasilprev.dto.ResgateForm;
import com.brasilprev.brasilprev.dto.ResgateResponse;
import com.brasilprev.brasilprev.service.ResgateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/resgates")
public class ResgateController {

    @Autowired
    private ResgateService resgateService;

    @PostMapping
    public ResponseEntity<ResgateResponse> insert(
            @RequestBody @Valid ResgateForm model) {
        return ResponseEntity.status(HttpStatus.CREATED).body(resgateService.insert(model));

    }
}
