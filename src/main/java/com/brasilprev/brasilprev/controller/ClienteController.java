package com.brasilprev.brasilprev.controller;

import com.brasilprev.brasilprev.dto.ClienteResponse;
import com.brasilprev.brasilprev.dto.ClienteForm;
import com.brasilprev.brasilprev.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    public ResponseEntity<ClienteResponse> insert(
            @RequestBody @Valid ClienteForm model) {
        return ResponseEntity.status(HttpStatus.CREATED).body(clienteService.insert(model));

    }
}
