package com.brasilprev.brasilprev.controller;

import com.brasilprev.brasilprev.dto.AporteExtraForm;
import com.brasilprev.brasilprev.dto.AporteExtraResponse;
import com.brasilprev.brasilprev.service.AporteExtraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/aporteExtras")
public class AporteExtraController {

    @Autowired
    private AporteExtraService aporteExtraService;

    @PostMapping
    public ResponseEntity<AporteExtraResponse> insert(
            @RequestBody @Valid AporteExtraForm model) {
        return ResponseEntity.status(HttpStatus.CREATED).body(aporteExtraService.insert(model));

    }
}
