package com.brasilprev.brasilprev.controller;

import com.brasilprev.brasilprev.dto.PlanoForm;
import com.brasilprev.brasilprev.dto.PlanoResponse;
import com.brasilprev.brasilprev.service.PlanoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/planos")
public class PlanoController {

    @Autowired
    private PlanoService planoService;

    @PostMapping
    public ResponseEntity<PlanoResponse> insert(
            @RequestBody @Valid PlanoForm model) {
        return ResponseEntity.status(HttpStatus.CREATED).body(planoService.insert(model));

    }
}
