package com.brasilprev.brasilprev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class BrasilprevApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrasilprevApplication.class, args);
	}

}
