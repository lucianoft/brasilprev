package com.brasilprev.brasilprev.repository;

import com.brasilprev.brasilprev.entity.AporteExtra;
import com.brasilprev.brasilprev.entity.Produto;
import org.springframework.data.repository.CrudRepository;

public interface AporteExtraRepository extends CrudRepository<AporteExtra, Long> {
}
