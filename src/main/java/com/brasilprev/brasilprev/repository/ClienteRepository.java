package com.brasilprev.brasilprev.repository;

import com.brasilprev.brasilprev.entity.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {
}
