package com.brasilprev.brasilprev.repository;

import com.brasilprev.brasilprev.entity.Produto;
import org.springframework.data.repository.CrudRepository;

public interface ProdutoRepository extends CrudRepository<Produto, Long> {
}
