package com.brasilprev.brasilprev.repository;

import com.brasilprev.brasilprev.entity.Plano;
import com.brasilprev.brasilprev.entity.Produto;
import org.springframework.data.repository.CrudRepository;

public interface PlanoRepository extends CrudRepository<Plano, Long> {
}
