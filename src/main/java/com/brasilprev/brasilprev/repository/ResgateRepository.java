package com.brasilprev.brasilprev.repository;

import com.brasilprev.brasilprev.entity.Resgate;
import org.springframework.data.repository.CrudRepository;

public interface ResgateRepository extends CrudRepository<Resgate, Long> {

    Resgate findTop1ByPlanoId(Long idPlano);

}
