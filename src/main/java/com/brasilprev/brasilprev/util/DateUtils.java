package com.brasilprev.brasilprev.util;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;

public class DateUtils {

    public static boolean after(LocalDate dtRef1, LocalDate dtRef2) {
        boolean isAfter = false;
        if (dtRef1 != null && dtRef2 != null) {
            isAfter = dtRef1.isAfter(dtRef2);
        }
        return isAfter;
    }

    public static int idade(final LocalDate aniversario) {
        return Period.between(aniversario, LocalDate.now()).getYears();
    }

    public static int idade(final int dia, final int mes, final int ano) {
        return idade(LocalDate.of(ano, mes, dia));
    }

    public static long dateDiffDay(LocalDate date, LocalDate anotherDate) {
        if (date != null && anotherDate != null) {
            return ChronoUnit.DAYS.between(anotherDate, date);
        }
        return 0;
    }
}
