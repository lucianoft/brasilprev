package com.brasilprev.brasilprev.util;

import java.math.BigDecimal;

public class NumberUtils {

    public static boolean isGreaterThan(BigDecimal value, BigDecimal valueDe) {
        if (value == null || valueDe == null) {
            return false;
        }
        if (value.compareTo(valueDe) > 0) {
            return true;
        }
        return false;
    }

    public static boolean isLessThan(BigDecimal num1, BigDecimal num2) {

        num1 = coalesce(num1, BigDecimal.ZERO);
        num2 = coalesce(num2, BigDecimal.ZERO);

        return num1.compareTo(num2) < 0;
    }

    public static BigDecimal coalesce(BigDecimal value, BigDecimal defaultValue) {
        if (value != null) {
            return value;
        }
        return defaultValue;
    }

}
