FROM openjdk:11-jre
VOLUME /tmp

#RUN apk update && apk add bash

ARG JAR_FILE

COPY target/brasilprev-api.jar brasilprev-api/app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","-verbose:gc","brasilprev-api/app.jar"]


